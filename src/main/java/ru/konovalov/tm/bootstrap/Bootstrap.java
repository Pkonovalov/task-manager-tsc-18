package ru.konovalov.tm.bootstrap;

import ru.konovalov.tm.api.repository.ICommandRepository;
import ru.konovalov.tm.api.repository.IProjectRepository;
import ru.konovalov.tm.api.repository.ITaskRepository;
import ru.konovalov.tm.api.repository.IUserRepository;
import ru.konovalov.tm.api.service.*;
import ru.konovalov.tm.command.AbstractCommand;
import ru.konovalov.tm.command.project.*;
import ru.konovalov.tm.command.system.*;
import ru.konovalov.tm.command.task.*;
import ru.konovalov.tm.command.user.*;
import ru.konovalov.tm.enumerated.Role;
import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.exeption.system.UnknownCommandException;
import ru.konovalov.tm.model.Project;
import ru.konovalov.tm.model.Task;
import ru.konovalov.tm.repository.CommandRepository;
import ru.konovalov.tm.repository.ProjectRepository;
import ru.konovalov.tm.repository.TaskRepository;
import ru.konovalov.tm.repository.UserRepository;
import ru.konovalov.tm.service.*;
import ru.konovalov.tm.util.TerminalUtil;

public class Bootstrap implements ServiceLocator {

    private final IProjectRepository projectRepository = new ProjectRepository();
    private final ITaskRepository taskRepository = new TaskRepository();
    private final ICommandRepository commandRepository = new CommandRepository();
    private final IUserRepository userRepository = new UserRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ICommandService commandService = new CommandService(commandRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final ILoggerService loggerService = new LoggerService();
    private final IUserService userService = new UserService(userRepository);
    private final IAuthService authService = new AuthService(userService);


    {
        registry(new ProjectByIdFinishCommand());
        registry(new ProjectByIdRemoveCommand());
        registry(new ProjectByIdSetStatusCommand());
        registry(new ProjectByIdStartCommand());
        registry(new ProjectByIdUpdateCommand());
        registry(new ProjectByIdViewCommand());
        registry(new ProjectByIndexFinishCommand());
        registry(new ProjectByIndexRemoveCommand());
        registry(new ProjectByIndexSetStatusCommand());
        registry(new ProjectByIndexStartCommand());
        registry(new ProjectByIndexUpdateCommand());
        registry(new ProjectByIndexViewCommand());
        registry(new ProjectByNameFinishCommand());
        registry(new ProjectByNameRemoveCommand());
        registry(new ProjectByNameSetStatusCommand());
        registry(new ProjectByNameStartCommand());
        registry(new ProjectByNameUpdateCommand());
        registry(new ProjectByNameViewCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());

        registry(new TaskByIdFinishCommand());
        registry(new TaskByIdRemoveCommand());
        registry(new TaskByIdSetStatusCommand());
        registry(new TaskByIdStartCommand());
        registry(new TaskByIdUnbindCommand());
        registry(new TaskByIdUpdateCommand());
        registry(new TaskByIdViewCommand());
        registry(new TaskByIndexFinishCommand());
        registry(new TaskByIndexRemoveCommand());
        registry(new TaskByIndexSetStatusCommand());
        registry(new TaskByIndexStartCommand());
        registry(new TaskByIndexUpdateCommand());
        registry(new TaskByIndexViewCommand());
        registry(new TaskByNameFinishCommand());
        registry(new TaskByNameRemoveCommand());
        registry(new TaskByNameSetStatusCommand());
        registry(new TaskByNameStartCommand());
        registry(new TaskByNameUpdateCommand());
        registry(new TaskByNameViewCommand());
        registry(new TaskByNameViewCommand());
        registry(new TaskByProjectIdBindCommand());
        registry(new TaskByProjectIdListCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListCommand());

        registry(new AboutCommand());
        registry(new ArgumentsCommand());
        registry(new CommandsCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new SysinfoCommand());
        registry(new VersionCommand());

        registry(new UserChangePasswordCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserUpdateCommand());
        registry(new UserViewProfileCommand());

    }

    public void run(final String[] args) {
        loggerService.info("*** WeLcOmE To TaSk MaNaGeR ***");
        if (parseArgs(args)) System.exit(0);
        initDate();
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAILED]");
            }
        }
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initDate() {
        String adminId = userService.add("admin", "admin", "admin@a").setRole(Role.ADMIN).getId();
        userService.add("user", "user");

        projectService.add(adminId, new Project("Project C", "-")).setStatus(Status.COMPLETE);
        projectService.add(adminId, new Project("Project A", "-"));
        projectService.add(adminId, new Project("Project B", "-")).setStatus(Status.IN_PROGRESS);
        projectService.add(adminId, new Project("Project D", "-")).setStatus(Status.COMPLETE);
        taskService.add(adminId, new Task("Task C", "-")).setStatus(Status.COMPLETE);
        taskService.add(adminId, new Task("Task A", "-"));
        taskService.add(adminId, new Task("Task B", "-")).setStatus(Status.IN_PROGRESS);
        taskService.add(adminId, new Task("Task D", "-")).setStatus(Status.COMPLETE);
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }


    public void parseCommand(final String com) {
        if (com == null) return;
        final AbstractCommand command = commandService.getCommandByName(com);
        if (command == null) throw new UnknownCommandException(com);
        command.execute();
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }


    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }
}