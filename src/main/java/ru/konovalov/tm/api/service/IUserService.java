package ru.konovalov.tm.api.service;

import ru.konovalov.tm.model.User;

import java.util.List;

public interface IUserService {
    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User remove(User user);

    User removeById(String id);

    User removeByLogin(String login);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    User add(String login, String password);

    User add(String login, String password, String email);

    User setPassword(String id, String password);

    User updateUser(
            String id,
            String firstName,
            String lastName,
            String middleName
    );

    boolean existsByLogin(String login);
}
