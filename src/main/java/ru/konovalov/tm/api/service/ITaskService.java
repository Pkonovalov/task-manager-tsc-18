package ru.konovalov.tm.api.service;

import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.model.Project;
import ru.konovalov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {


    List<Task> findAll(String userId);

    List<Task> findAll(String userId, Comparator<Task> comparator);

    Task add(String userId, Task task);

    void remove(String userId, Task task);

    void clear(String userId);

    Task removeOneById(String userId, String id);

    Task findOneById(String userId, String id);

    Task findOneByName(String userId, String name);

    Task removeOneByName(String userId, String name);

    Task removeTaskByIndex(String userId, Integer index);

    Task findOneByIndex(String userId, Integer index);

    Task updateTaskByIndex(String userId, Integer index, String name, String description);

    Task updateTaskByName(String userId, String name, String nameNew, String description);

    Task updateTaskById(String userId, String id, String name, String description);

    Task finishTaskById(String userId, String id);

    Task finishTaskByName(String userId, String name);

    Task startTaskById(String userId, String id);

    Task startTaskByIndex(String userId, Integer index);

    Task startTaskByName(String userId, String name);

    Task changeTaskStatusById(String userId, String id, Status status);

    Task changeTaskStatusByName(String userId, String name, Status status);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

    String getIdByIndex(String userId, Integer index);

    int size(String userId);

    boolean existsByName(String userId, String name);
}
