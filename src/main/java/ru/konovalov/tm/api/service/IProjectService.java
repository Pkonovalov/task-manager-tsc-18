package ru.konovalov.tm.api.service;

import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll(final String userId);

    List<Project> findAll(final String userId, final Comparator<Project> comparator);

    Project findOneById(String userId, String id);

    Project add(String userId, Project project);

    void remove(String userId, Project project);

    void clear(String userId);

    Project removeOneById(String userId, String id);

    Project findOneByName(String userId, String name);

    Project removeOneByName(String userId, String name);

    Project removeProjectByIndex(String userId, Integer index);

    Project findOneByIndex(String userId, Integer index);

    Project updateProjectByIndex(String userId, Integer index, String name, String description);

    Project updateProjectById(String userId, String id, String name, String description);

    Project updateProjectByName(String userId, String name, String nameNew, String description);

    Project startProjectById(String userId, String id);

    Project startProjectByIndex(String userId, Integer index);

    Project startProjectByName(String userId, String name);

    Project finishProjectById(String userId, String id);

    Project finishProjectByIndex(String userId, Integer index);

    Project finishProjectByName(String userId, String name);

    Project changeProjectStatusById(String userId, String id, Status status);

    Project changeProjectStatusByName(String userId, String name, Status status);

    Project changeProjectStatusByIndex(String userId, Integer index, Status status);

    boolean existsById(String id);

    boolean existsByName(String userId, String name);

    int size(String userId);
}
