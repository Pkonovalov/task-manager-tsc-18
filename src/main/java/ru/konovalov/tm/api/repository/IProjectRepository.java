package ru.konovalov.tm.api.repository;

import ru.konovalov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll(String userId);

    List<Project> findAll(String userId, Comparator<Project> comparator);

    void add(String userId, Project project);

    void remove(String userId, Project project);

    void clear(String userId);

    Project findOneById(String userId, String id);

    Project removeOneById(String userId, String id);

    Project findOneByIndex(String userId, Integer index);

    Project removeOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project removeOneByName(String userId, String name);

    boolean existsByName(String userId, String name);

    boolean existsById(String id);

    int size(String userId);

    String getIdByName(String userId, String name);

    String getIdByIndex(String userId, Integer index);
}
