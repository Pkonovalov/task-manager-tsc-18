package ru.konovalov.tm.service;

import ru.konovalov.tm.api.repository.IProjectRepository;
import ru.konovalov.tm.api.service.IProjectTaskService;
import ru.konovalov.tm.api.repository.ITaskRepository;
import ru.konovalov.tm.exeption.empty.EmptyIdException;
import ru.konovalov.tm.exeption.empty.EmptyNameException;
import ru.konovalov.tm.exeption.entity.ProjectNotFoundException;
import ru.konovalov.tm.model.Project;
import ru.konovalov.tm.model.Task;

import java.util.List;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService( IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findALLTaskByProjectId(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return taskRepository.findALLTaskByProjectId(userId, projectId);
    }

    @Override
    public Task assignTaskByProjectId(final String userId,final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        return taskRepository.assignTaskByProjectId(userId, projectId, taskId);
    }

    @Override
    public Task unassignTaskByProjectId(final String userId, final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        return taskRepository.unassignTaskByProjectId(userId,taskId);
    }

    @Override
    public List<Task> removeTasksByProjectId(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return taskRepository.removeAllTaskByProjectId(userId,projectId);
    }

    @Override
    public Project removeProjectById(final String userId, final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeOneById(userId, projectId);
        return null;
    }

    @Override
    public void removeProjectByName(final String userId,final String projectName) {
        if (isEmpty(projectName)) throw new EmptyNameException();
        String projectId = projectRepository.getIdByName(userId,projectName);
        if (isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllTaskByProjectId(userId,projectId);
        projectRepository.removeOneByName(userId,projectName);
    }


}
