package ru.konovalov.tm.service;

import ru.konovalov.tm.api.repository.ITaskRepository;
import ru.konovalov.tm.api.service.ITaskService;
import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.exeption.empty.EmptyIdException;
import ru.konovalov.tm.exeption.empty.EmptyIndexException;
import ru.konovalov.tm.exeption.empty.EmptyNameException;
import ru.konovalov.tm.exeption.system.IndexIncorrectException;
import ru.konovalov.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static ru.konovalov.tm.util.ValidationUtil.checkIndex;
import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll(final String userId) {
        return taskRepository.findAll(userId);
    }

    @Override
    public List<Task> findAll(final String userId, final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(userId, comparator);
    }


    @Override
    public Task add(final String userId, final Task task) {
        if (task == null) return null;
        taskRepository.add(userId, task);
        return task;
    }

    @Override
    public void remove(final String userId, final Task task) {
        if (task == null) return;
        taskRepository.remove(userId, task);
    }

    @Override
    public void clear(String userId) {
        taskRepository.clear(userId);
    }

    @Override
    public Task removeOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeOneById(userId, id);
    }

    @Override
    public Task findOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findOneById(userId, id);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    public Task removeOneByName(String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeOneByName(userId, name);
    }

    @Override
    public Task removeTaskByIndex(String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Task findOneByIndex(String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > taskRepository.size(userId)) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @Override
    public Task updateTaskByIndex(String userId, final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByName(String userId, final String name, final String nameNew, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyIndexException();
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        task.setName(nameNew);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskById(final String userId, final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task finishTaskById(String userId, String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task finishTaskByName(String userId, String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task startTaskById(String userId, String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(String userId, Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task startTaskByName(String userId, String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task changeTaskStatusById(String userId, String id, Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(String userId, String name, Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(String userId, Integer index, Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public String getIdByIndex(String userId, Integer index) {
        if (!checkIndex(index, taskRepository.size(userId))) throw new IndexIncorrectException();
        return taskRepository.getIdByIndex(index);
    }

    @Override
    public int size(final String userId) {
        return taskRepository.size(userId);
    }

    public boolean existsByName(String userId, String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return taskRepository.existsByName(userId, name);
    }

}
