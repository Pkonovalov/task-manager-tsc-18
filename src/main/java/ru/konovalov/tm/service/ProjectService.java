package ru.konovalov.tm.service;

import ru.konovalov.tm.api.repository.IProjectRepository;
import ru.konovalov.tm.api.service.IProjectService;
import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.exeption.empty.EmptyIdException;
import ru.konovalov.tm.exeption.empty.EmptyIndexException;
import ru.konovalov.tm.exeption.empty.EmptyNameException;
import ru.konovalov.tm.exeption.entity.ProjectNotFoundException;
import ru.konovalov.tm.exeption.system.IndexIncorrectException;
import ru.konovalov.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll(final String userId) {
        return projectRepository.findAll(userId);
    }

    @Override
    public List<Project> findAll(final String userId, final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(userId, comparator);
    }

    @Override
    public Project findOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneById(userId, id);
    }

    @Override
    public Project add(final String userId, final Project project) {
        if (project == null) return null;
        projectRepository.add(userId, project);
        return project;
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (project == null) return;
        projectRepository.remove(userId, project);
    }

    @Override
    public void clear(final String userId) {
        projectRepository.clear(userId);
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeOneById(userId, id);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }


    @Override
    public Project removeOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeOneByName(userId, name);
    }

    @Override
    public Project removeProjectByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (index > projectRepository.size(userId)) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Override
    public Project updateProjectByIndex(final String userId, final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectById(final String userId, final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }
    @Override
    public Project updateProjectByName(final String userId, final String name, final String nameNew, final String description) {
        if (isEmpty(name) || isEmpty(nameNew)) throw new EmptyNameException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(nameNew);
        project.setDescription(description);
        return project;
    }


    @Override
    public Project startProjectById(String userId, String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findOneById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;
    }

    @Override
    public Project startProjectByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = projectRepository.findOneByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;

    }

    @Override
    public Project startProjectByName(String userId, String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findOneByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;
    }

    @Override
    public Project finishProjectById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findOneById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateStart(new Date());
        return project;
    }

    @Override
    public Project finishProjectByIndex(final String userId, Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = projectRepository.findOneByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateStart(new Date());
        return project;
    }

    @Override
    public Project finishProjectByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findOneByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateStart(new Date());
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String userId, final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByName(final String userId, final String name, final Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(userId, name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId, final Integer index, final Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = findOneByIndex(userId,index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public boolean existsById(String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return projectRepository.existsById(id);
    }


    @Override
    public boolean existsByName(String userId, String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return projectRepository.existsByName(userId, name);
    }

    @Override
    public int size(final String userId) {
        return projectRepository.size(userId);
    }

}

