package ru.konovalov.tm.command.system;

import ru.konovalov.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String description() {
        return "Show developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Petr Konovalov");
        System.out.println("pkonovalov@tsconsulting.com");
    }
}
