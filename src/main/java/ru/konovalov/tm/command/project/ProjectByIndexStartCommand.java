package ru.konovalov.tm.command.project;

import ru.konovalov.tm.util.TerminalUtil;

public class ProjectByIndexStartCommand extends AbstractProjectCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-start-by-index";
    }

    @Override
    public String description() {
        return "Start project by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[START PROJECT]");
        System.out.println("ENTER INDEX:");
        serviceLocator.getProjectService().startProjectByIndex(userId, TerminalUtil.nextNumber() - 1);
    }

}
