package ru.konovalov.tm.command.project;

import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.exeption.entity.ProjectNotFoundException;
import ru.konovalov.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectByIdSetStatusCommand extends AbstractProjectCommand {


    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-change-status-by-id";
    }

    @Override
    public String description() {
        return "Change project status by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SET PROJECT STATUS]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        if (!serviceLocator.getProjectService().existsById(id)) throw new ProjectNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        serviceLocator.getProjectService().changeProjectStatusById(userId, id, Status.getStatus(TerminalUtil.nextLine()));
    }


}

