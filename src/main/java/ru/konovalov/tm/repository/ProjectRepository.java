package ru.konovalov.tm.repository;

import ru.konovalov.tm.api.repository.IProjectRepository;
import ru.konovalov.tm.exeption.entity.ProjectNotFoundException;
import ru.konovalov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public List<Project> findAll(final String userId) {
        List<Project> list = new ArrayList<>();
        for (Project project : this.projects) {
            if (userId.equals(project.getUserId())) list.add(project);
        }
        return list;
    }

    @Override
    public List<Project> findAll(final String userId, Comparator<Project> comparator) {
        final List<Project> projects = new ArrayList<>(findAll(userId));
        projects.sort(comparator);
        return projects;
    }

    @Override
    public void add(final String userId, final Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (!userId.equals(project.getUserId())) return;
        projects.remove(project);
    }

    @Override
    public void clear(final String userId) {
        List<Project> list = findAll(userId);
        this.projects.removeAll(list);
    }
    @Override
    public Project findOneById(final String userId, final String id) {
        for (final Project project : projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        final Project project = findOneById(userId, id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        List<Project> projects = findAll(userId);
        return projects.get(index);
    }

    @Override
    public Project removeOneByIndex(final String userId, Integer index) {
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        for (final Project project : projects) {
            if (!userId.equals(project.getUserId())) continue;
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneByName(final String userId, final String name) {
        final Project project = findOneByName(userId, name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public boolean existsByName(final String userId, final String name) {
        for (final Project project : projects) {
            if (name.equals(project.getName()) && userId.equals(project.getName())) return true;
        }
        return false;
    }

    @Override
    public boolean existsById(String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return true;
        }
        return false;
    }

    @Override
    public int size(final String userId) {
        List<Project> list = findAll(userId);
        return list.size();
    }

    @Override
    public String getIdByName(final String userId, final String name) {
        for (final Project project : projects) {
            if (name.equals(project.getName())) return project.getId();
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public String getIdByIndex(final String userId, final Integer index) {
        int i = 0;
        for (final Project project : projects) {
            if (userId.equals(project.getUserId())) i++;
            if (index.equals(i)) return project.getId();
        }
        throw new ProjectNotFoundException();
    }

}


